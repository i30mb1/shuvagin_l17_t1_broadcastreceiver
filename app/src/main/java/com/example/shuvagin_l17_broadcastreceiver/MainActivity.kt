package com.example.shuvagin_l17_broadcastreceiver

import android.Manifest.permission.READ_CALL_LOG
import android.Manifest.permission.READ_PHONE_STATE
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.BatteryManager
import android.os.Bundle
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.text.SpannableStringBuilder
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.text.color
import androidx.databinding.DataBindingUtil
import com.example.shuvagin_l17_broadcastreceiver.databinding.ItemReceiverBinding
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {

    var isCharging: Boolean = false

    val br: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Intent.ACTION_BATTERY_CHANGED -> {
                    val status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1)
                    val currentState: Boolean = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL
                    if (isCharging != currentState) {
                        if (!isCharging) addEntry(getString(R.string.main_activity_plugged)) else addEntry(getString(R.string.main_activity_unplugged))
                        isCharging = currentState
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        askPermission()
    }

    override fun onResume() {
        super.onResume()
        regBatteryStatus()
        regTelephoneManager()
    }

    private fun regBatteryStatus() {
        val batteryStatus = IntentFilter().apply {
            addAction(Intent.ACTION_BATTERY_CHANGED)
        }
        registerReceiver(br, batteryStatus)
    }

    private fun regTelephoneManager() {
        val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        telephonyManager.listen(MyPhoneStateListener(), PhoneStateListener.LISTEN_CALL_STATE)
    }

    override fun onPause() {
        unregisterReceiver(br)
        super.onPause()
    }

    inner class MyPhoneStateListener : PhoneStateListener() {
        override fun onCallStateChanged(state: Int, incomingNumber: String) {
            super.onCallStateChanged(state, incomingNumber)
            if (incomingNumber.isNotEmpty() && state == TelephonyManager.CALL_STATE_RINGING) {
                addEntry(getString(R.string.main_activity_incoming_number, incomingNumber))
            }
        }
    }

    private fun askPermission() {
        if (ContextCompat.checkSelfPermission(this, READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(READ_CALL_LOG, READ_PHONE_STATE), 0)
        }
    }

    fun addEntry(status: String) {
        val layout: ItemReceiverBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.item_receiver,
            root_activity_main,
            true
        )
        layout.tvItemReceiverMessage.text = status

        val calendar = Calendar.getInstance(TimeZone.getDefault())
        val minutes = calendar.get(Calendar.MINUTE)
        val hours = calendar.get(Calendar.HOUR_OF_DAY)

//        layout.tvItemReceiverTime.text = String.format("%d:%d", hours, minutes)
        layout.tvItemReceiverTime.text = SpannableStringBuilder(hours.toString())
            .color(ContextCompat.getColor(this, R.color.colorAccent)) { append(":") }
            .append(minutes.toString())
    }
}
